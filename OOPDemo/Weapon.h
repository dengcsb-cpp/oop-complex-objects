#pragma once
#include <string>
// Included since we need to print the weapon
#include <iostream>
using namespace std;

class Weapon
{
public:
	Weapon(string name, int power);

	// Didn't include a setter. 
	// Values can only be set during initialization.
	int getPower();
	string getName();

	void printStats();

private:
	string mName;
	int mPower;
};
