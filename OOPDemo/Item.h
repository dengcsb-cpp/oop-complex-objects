#pragma once
#include <string>
#include <iostream>
using namespace std;

class Archer;

class Item
{
public:
	Item(string name, int healValue);

	// Getters
	string getName();
	int getHealValue();

	// When an item is used, it is the Item that is performing the action
	void use(Archer* target);

private:
	string mName;
	int mHealValue;
};
