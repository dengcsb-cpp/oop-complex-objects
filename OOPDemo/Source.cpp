#include <iostream>
#include <string>
#include "Archer.h"
#include "Item.h"
#include "Weapon.h"

using namespace std;

int main()
{
	// Create the player
	Archer* player = new Archer("Archer1", 10, 100);
	// Set weapon
	Weapon* weapon = new Weapon("Hunter Bow", 12);
	// Add items. Each item should be instantiated independently
	// I didn't bother storing the item in a pointer variable
	// since it will be owned by the Archer anyway.
	player->addItem(new Item("Potion", 20));
	player->addItem(new Item("Potion", 20));
	player->addItem(new Item("Potion", 20));

	// Create the enemy archer (test target)
	Archer* enemy = new Archer("Archer2", 10, 100);
	
	// Test attack. This should include the weapon's power
	player->attack(enemy);

	// Test use item. This should use the item and remove it from the vector
	player->useItem(0);

	// Delete the archers
	delete player;
	delete enemy;
}
