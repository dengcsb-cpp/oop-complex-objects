#include "Item.h"
// Don't forget to include any forward-declared classes
#include "Archer.h"

Item::Item(string name, int healValue)
{
	mName = name;
	mHealValue = healValue;
}

string Item::getName()
{
	return mName;
}

int Item::getHealValue()
{
	return mHealValue;
}

void Item::use(Archer* target)
{
	target->heal(mHealValue);
	cout << target->getName() << " used " << mName << "! HP +" << mHealValue << "!";
}
