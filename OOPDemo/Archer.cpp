#include "Archer.h"
// Include any forward-declared types in the header file here
#include "Item.h"
#include "Weapon.h"

Archer::Archer(string name, int power, int hp)
{
	mName = name;
	mPower = power;
	mHp = mHp;

	// Default weapon
	mWeapon = new Weapon("Practice Bow", 5);
}

Archer::~Archer()
{
	// Deallocate weapon
	// No need to set to null. This instance is about to be destroyed anyway.
	delete mWeapon;

	// Deallocate items.
	// We have to delete them one-by-one
	for (int i = 0; i < mItems.size(); i++)
	{
		delete mItems[i];
	}
	// Remove all items in the inventory
	mItems.clear();
}

void Archer::attack(Archer* target)
{
	// Compute damage based on archer's Power and Weapon
	int damage = mPower + mWeapon->getPower();

	target->takeDamage(damage);
	cout << mName << " attacked " << target->getName() << ". " << mPower << " damage!" << endl;
}

void Archer::strafe()
{
	cout << "Strafe used!" << endl;
}

void Archer::setName(string name)
{
	mName = name;
}

void Archer::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Archer::heal(int value)
{
	if (value < 1) return;
	mHp += value;
}

void Archer::setPower(int power)
{
	mPower = power;
}

Weapon* Archer::getWeapon()
{
	return mWeapon;
}

void Archer::equipWeapon(Weapon* weapon)
{
	// Delete the previous weapon, if any
	if (mWeapon != nullptr) delete mWeapon;

	mWeapon = weapon;
}

void Archer::unequipWeapon()
{
	// Delete the previous weapon, if any
	if (mWeapon != nullptr) delete mWeapon;

	// Null weapon means no weapon is equipped
	mWeapon = nullptr;
}

vector<Item*>& Archer::getItems()
{
	return mItems;
}

void Archer::addItem(Item* item)
{
	mItems.push_back(item);
}

void Archer::useItem(int index)
{
	// Check if index is valid
	if (index > mItems.size() - 1)
	{
		cout << "Item does not exist!" << endl;
		return;
	}

	// Use the item
	// 'this' represents the Archer who owns this item. It's a pointer to self.
	mItems[index]->use(this);
	
	// Remove the item from the vector
	mItems.erase(mItems.begin() + index);
}

void Archer::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

string Archer::getName()
{
	return mName;
}

int Archer::getHp()
{
	return mHp;
}

int Archer::getPower()
{
	return mPower;
}

