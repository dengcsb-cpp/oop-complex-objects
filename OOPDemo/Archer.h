#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

// Don't forget to forward-declare any data type that uses a .h file
class Item;
class Weapon;

class Archer
{
public:
	Archer(string name, int power, int hp);
	// Destructor. There is only one of this!
	~Archer();

	void attack(Archer* target);
	void strafe();

public:
	string getName();
	void setName(string name);

	// Intuitive HP accessors
	void takeDamage(int damage);
	void heal(int value);

	int getHp();
	void setHp(int value);

	int getPower();
	void setPower(int power);

	// Accessors for complex properties
	Weapon* getWeapon();
	void equipWeapon(Weapon* weapon);
	void unequipWeapon();

	// Provide access to items through these
	// Get a list of all the items
	vector<Item*>& getItems();
	// Add items to inventory
	void addItem(Item* item);
	// Consume an item. The actor discards the item from inventory after use.
	void useItem(int index);

private:
	// Simple properties
	string mName;
	int mHp;
	int mPower;

	// Complex properties
	Weapon* mWeapon;
	vector<Item*> mItems;
};
