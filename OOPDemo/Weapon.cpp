#include "Weapon.h"

Weapon::Weapon(string name, int power)
{
	mName = name;
	mPower = power;
}

int Weapon::getPower()
{
	return mPower;
}

string Weapon::getName()
{
	return mName;
}

void Weapon::printStats()
{
	cout << mName << "\tPOW: " << mPower << endl;
}
